import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Formative2 {
    static String newText;
    static String numberFromText, alphabetFromText;

    public static String editText(String text){
        String lregex = "[L]";
        String iregex = "[i]";
        String pregex = "[p]";
        String oregex = "[o]";
        String sregex = "[s]";

        String result1 = text.replaceAll(lregex, String.valueOf(7));
        String result2 = result1.replaceAll(iregex, String.valueOf(1));
        String result3 = result2.replaceAll(pregex, String.valueOf(8));
        String result4 = result3.replaceAll(oregex, String.valueOf(9));
        String lastResult = result4.replaceAll(sregex, String.valueOf(9));

        newText = lastResult;

        return newText;
    }

    public static String getAlphabetFromText(){
        Pattern pattern = Pattern.compile("[0-9 .,]");
        Matcher matcher = pattern.matcher(newText);
        alphabetFromText = matcher.replaceAll("");
        return alphabetFromText;
    }

    public static String getNumberFromText(){
        Pattern pattern = Pattern.compile("[a-zA-z .,]");
        Matcher matcher = pattern.matcher(newText);
        numberFromText = matcher.replaceAll("");
        return numberFromText;
    }

    public static void createFileAlphabet(){
        try (BufferedWriter createFile = new BufferedWriter(new FileWriter("day822.txt"))){
            createFile.write(alphabetFromText);
            createFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createFileNumbers(){
        try (BufferedWriter createFile = new BufferedWriter(new FileWriter("day821.txt"))){
            createFile.write(numberFromText);
            createFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
