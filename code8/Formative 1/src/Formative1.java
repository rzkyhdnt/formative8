public class Formative1 {
    static String text;
    static String newText;

    public static String editText(String text){
        String lregex = "[L]";
        String iregex = "[i]";
        String pregex = "[p]";
        String oregex = "[o]";
        String sregex = "[s]";

        String result1 = text.replaceAll(lregex, String.valueOf(7));
        String result2 = result1.replaceAll(iregex, String.valueOf(1));
        String result3 = result2.replaceAll(pregex, String.valueOf(8));
        String result4 = result3.replaceAll(oregex, String.valueOf(9));
        String lastResult = result4.replaceAll(sregex, String.valueOf(9));

        newText = lastResult;

        return newText;
    }
}
