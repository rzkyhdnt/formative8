import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Netflix {
    String name, dateNow;
    int setReminder;
    LocalDate registDate = LocalDate.parse("2021-02-14");

    public Netflix() { }

    public Netflix(String name){
        this.name = name;
    }

    public void statusInvoiceDateNow(String dateNow){
        this.dateNow = dateNow;
        LocalDate date = LocalDate.parse(dateNow);
        int dayRegist = registDate.getDayOfMonth();
        int monthRegist = registDate.getMonthValue();
        int dayNow = date.getDayOfMonth();
        int monthNow = date.getMonthValue();
        int yearNow = date.getYear();
        int monthInvoice = monthNow - monthRegist;
        int dayInvoice = dayNow - dayRegist;

        for(int i=1; i <= monthInvoice; i++){
            if((monthInvoice >= 1 && dayNow >= 14) || (monthInvoice >= 1 && dayInvoice > 0)){
                System.out.println("\n---------NOTIFIKASI PEMBAYARAN--------");
                System.out.println("Halo, " + name + ".");
                System.out.println("Paket Netflix premium Rp. 153.000 akan diperpanjang otomatis dan " +
                        "saldo Anda akan otomatis berkurang. " + (dayNow) + "-" + (monthRegist+i) + "-" + yearNow);
            }
        }
    }

    public void setNotification(int setReminder){
        this.setReminder = setReminder;
        LocalDate date = LocalDate.parse(dateNow);
        String reminderDate = "2021-02-" + String.valueOf(setReminder);
        LocalDate remindDate = LocalDate.parse(reminderDate);

        for(int i=0; i <= date.lengthOfYear(); i++){
                if(registDate.plusDays(i).getDayOfMonth() == remindDate.getDayOfMonth()){
                    System.out.println("\n---------REMINDER--------");
                    System.out.println(remindDate.plusDays(i+2).format(DateTimeFormatter.ofPattern("dd-MMMM-yyyy")) +
                            " -> Email reminder dikirim ke pelanggan\n");
            }
        }
    }
}

