import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NewAccount extends Netflix{
    String name, registDate;

    public NewAccount(String name, String registDate) {
        this.name = name;
        this.registDate = registDate;
    }

    public void register(){
        LocalDate date = LocalDate.parse(registDate);
        int counter = 0;

        for(int i = 1; i <= 12; i++){
            counter++;
            if(counter < 4){
                System.out.println("Free Premium New Account 3 Bulan pertama! " +
                        date.plusMonths(i).format(DateTimeFormatter.ofPattern("dd-MMMM-yyyy")));
            } else {
                System.out.println("Free Premium habis. Lanjutkan pembayaran pada tanggal " +
                        date.plusMonths(i).format(DateTimeFormatter.ofPattern("dd-MMMM-yyyy")));
            }
        }
    }
}
